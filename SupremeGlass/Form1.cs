﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SupremeGlass
{
    public partial class Form1 : Form
    {
        private float _width, _height;
        public Form1()
        {
            InitializeComponent();
            _width = _height = 0f;
        }

        /// <summary>
        /// Call when you cange somting in the GUI
        /// </summary>
        private void UpdateCalcualtions()
        {
            string textBoxWidth = textBox2.Text;
            string textBoxHeight = textBox1.Text;

            float surface = _width * _height;
            float outline = (_width * 2) + (_height * 2);
            string glassType = string.Empty;
            string woodType = string.Empty;

            if (Single.TryParse(textBoxWidth, out _width))
                Console.WriteLine(_width);
            else
                Console.WriteLine("Unable to parse '{0}'.", textBoxWidth);

            if (Single.TryParse(textBoxHeight, out _height))
                Console.WriteLine(_height);
            else
                Console.WriteLine("Unable to parse '{0}'.", textBoxHeight);

            try
            {
                glassType = GlassTypeBox.SelectedItem.ToString();
            } catch (NullReferenceException) { }
            try
            {
                woodType = WoodTypeBox.SelectedItem.ToString();
            } catch (NullReferenceException) { }

            //MessageBox.Show(glassType + "|" + woodType);

            float result = _width * _height;

            Euros.Text = result.ToString();
        }

        private void GlassTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCalcualtions();
        }

        private void WoodTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCalcualtions();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateCalcualtions();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            UpdateCalcualtions();
        }

        private void SupremeGlass_Load(object sender, EventArgs e)
        {

        }
    }
}
