﻿using System.Windows.Forms;

namespace SupremeGlass
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GlassTypeLabel = new System.Windows.Forms.Label();
            this.WoodTypeLabel = new System.Windows.Forms.Label();
            this.WoodTypeBox = new System.Windows.Forms.ComboBox();
            this.GlassTypeBox = new System.Windows.Forms.ComboBox();
            this.TestImage = new System.Windows.Forms.PictureBox();
            this.TypesOfGlass = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Width = new System.Windows.Forms.Label();
            this.Height = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.x = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Totaal = new System.Windows.Forms.Label();
            this.Euros = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TestImage)).BeginInit();
            this.SuspendLayout();
            // 
            // GlassTypeLabel
            // 
            this.GlassTypeLabel.AutoSize = true;
            this.GlassTypeLabel.Font = new System.Drawing.Font("Lucida Console", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GlassTypeLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.GlassTypeLabel.Location = new System.Drawing.Point(63, 96);
            this.GlassTypeLabel.Name = "GlassTypeLabel";
            this.GlassTypeLabel.Size = new System.Drawing.Size(182, 28);
            this.GlassTypeLabel.TabIndex = 0;
            this.GlassTypeLabel.Text = "Glass Type";
            // 
            // WoodTypeLabel
            // 
            this.WoodTypeLabel.AutoSize = true;
            this.WoodTypeLabel.Font = new System.Drawing.Font("Lucida Console", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WoodTypeLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.WoodTypeLabel.Location = new System.Drawing.Point(289, 92);
            this.WoodTypeLabel.Name = "WoodTypeLabel";
            this.WoodTypeLabel.Size = new System.Drawing.Size(165, 28);
            this.WoodTypeLabel.TabIndex = 2;
            this.WoodTypeLabel.Text = "Wood Type";
            // 
            // WoodTypeBox
            // 
            this.WoodTypeBox.AccessibleName = "";
            this.WoodTypeBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.WoodTypeBox.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WoodTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.WoodTypeBox.FormattingEnabled = true;
            this.WoodTypeBox.Items.AddRange(new object[] {
            "Garbage",
            "Medium",
            "Rare",
            "Majestic",
            "Supreme"});
            this.WoodTypeBox.Location = new System.Drawing.Point(278, 127);
            this.WoodTypeBox.Name = "WoodTypeBox";
            this.WoodTypeBox.Size = new System.Drawing.Size(184, 28);
            this.WoodTypeBox.TabIndex = 3;
            this.WoodTypeBox.SelectedIndexChanged += new System.EventHandler(this.WoodTypeBox_SelectedIndexChanged);
            // 
            // GlassTypeBox
            // 
            this.GlassTypeBox.AccessibleName = "";
            this.GlassTypeBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GlassTypeBox.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GlassTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.GlassTypeBox.FormattingEnabled = true;
            this.GlassTypeBox.Items.AddRange(new object[] {
            "Standart",
            "Supreme",
            "Ultra Rare",
            "Majestic",
            "Ultra Supreme"});
            this.GlassTypeBox.Location = new System.Drawing.Point(61, 127);
            this.GlassTypeBox.Name = "GlassTypeBox";
            this.GlassTypeBox.Size = new System.Drawing.Size(184, 28);
            this.GlassTypeBox.TabIndex = 4;
            this.GlassTypeBox.Tag = "test";
            this.GlassTypeBox.SelectedIndexChanged += new System.EventHandler(this.GlassTypeBox_SelectedIndexChanged);
            // 
            // TestImage
            // 
            this.TestImage.Image = global::SupremeGlass.Properties.Resources.tumblr_aesthetics_hippie_alternative_Favim_com_4284951;
            this.TestImage.Location = new System.Drawing.Point(615, 391);
            this.TestImage.Name = "TestImage";
            this.TestImage.Size = new System.Drawing.Size(435, 270);
            this.TestImage.TabIndex = 6;
            this.TestImage.TabStop = false;
            // 
            // TypesOfGlass
            // 
            this.TypesOfGlass.AutoSize = true;
            this.TypesOfGlass.Font = new System.Drawing.Font("Lucida Console", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TypesOfGlass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.TypesOfGlass.Location = new System.Drawing.Point(25, 23);
            this.TypesOfGlass.Name = "TypesOfGlass";
            this.TypesOfGlass.Size = new System.Drawing.Size(498, 33);
            this.TypesOfGlass.TabIndex = 7;
            this.TypesOfGlass.Text = "Types of Glass and Wood";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(632, 351);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(402, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Test image for Glass Windows";
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBox1.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Magenta;
            this.textBox1.Location = new System.Drawing.Point(571, 128);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(172, 24);
            this.textBox1.TabIndex = 9;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBox2.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Magenta;
            this.textBox2.Location = new System.Drawing.Point(839, 129);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(172, 24);
            this.textBox2.TabIndex = 10;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Width
            // 
            this.Width.AutoSize = true;
            this.Width.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Width.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Width.Location = new System.Drawing.Point(814, 92);
            this.Width.Name = "Width";
            this.Width.Size = new System.Drawing.Size(220, 24);
            this.Width.TabIndex = 11;
            this.Width.Text = "Width ( in cm )";
            // 
            // Height
            // 
            this.Height.AutoSize = true;
            this.Height.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Height.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Height.Location = new System.Drawing.Point(542, 92);
            this.Height.Name = "Height";
            this.Height.Size = new System.Drawing.Size(234, 24);
            this.Height.TabIndex = 12;
            this.Height.Text = "Height ( in cm )";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(612, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 13;
            // 
            // x
            // 
            this.x.AutoSize = true;
            this.x.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.x.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.x.Location = new System.Drawing.Point(780, 126);
            this.x.Name = "x";
            this.x.Size = new System.Drawing.Size(24, 24);
            this.x.TabIndex = 14;
            this.x.Text = "x";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(791, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 15;
            // 
            // Totaal
            // 
            this.Totaal.AutoSize = true;
            this.Totaal.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Totaal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Totaal.Location = new System.Drawing.Point(588, 230);
            this.Totaal.Name = "Totaal";
            this.Totaal.Size = new System.Drawing.Size(332, 24);
            this.Totaal.TabIndex = 16;
            this.Totaal.Text = "Totaal bedrag in euros:";
            // 
            // Euros
            // 
            this.Euros.AutoSize = true;
            this.Euros.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Euros.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Euros.Location = new System.Drawing.Point(926, 230);
            this.Euros.Name = "Euros";
            this.Euros.Size = new System.Drawing.Size(80, 24);
            this.Euros.TabIndex = 17;
            this.Euros.Text = "00,00";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1062, 669);
            this.Controls.Add(this.Euros);
            this.Controls.Add(this.Totaal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.x);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Height);
            this.Controls.Add(this.Width);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TypesOfGlass);
            this.Controls.Add(this.TestImage);
            this.Controls.Add(this.GlassTypeBox);
            this.Controls.Add(this.WoodTypeBox);
            this.Controls.Add(this.WoodTypeLabel);
            this.Controls.Add(this.GlassTypeLabel);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SupremeGlass";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SupremeGlass_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TestImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label GlassTypeLabel;
        private System.Windows.Forms.Label WoodTypeLabel;
        private System.Windows.Forms.ComboBox WoodTypeBox;
        private System.Windows.Forms.ComboBox GlassTypeBox;
        private System.Windows.Forms.PictureBox TestImage;
        private System.Windows.Forms.Label TypesOfGlass;
        private System.Windows.Forms.Label label1;
        private TextBox textBox1;
        private TextBox textBox2;
        private Label Width;
        private Label Height;
        private Label label2;
        private Label x;
        private Label label3;
        private Label Totaal;
        private Label Euros;
    }
}

